provider "aws" {
    region = "us-east-1"
    # access_key and secret are refernced from .aws/credentials file. Configure this using aws configure cmd
}


# defining variable

variable "subnet_cidr_block" {
    description = "subnet cidr block"
    # default values are apply if the vairable values are missing from the vars file
    # vars file values will overide the default value
    default = "10.0.10.0/24"
  
}

variable "vpc_cidr_block" {
    description = "VPC CIDR block"
  
}

# list of variables
variable "cidr_block" {
    description = "CIDR Block List"
    
    # define variable type
    type = list(string)
}

# list of object variables

variable "prod_cidr_block_object" {
    description = "List of Object"

    type = list(object ({
        cidr_block = string
        name = string,
    }))
}

# terraform global envirment vairables
# set terraform envirment vairables using export in command line
# For Example export TF_VAR_avail_zone="us-east-1a"
# call envirnment variable
# when calling envirnment variable only use the name leave out the TF_VAR extension

variable "avail_zone" {}

# create resource
resource "aws_vpc" "development-vpc" {
    cidr_block = var.vpc_cidr_block

    tags = {
      "Name" = "development",
    }
}

resource "aws_subnet" "dev-subnet-1" {
    # accessing the vpc id of the development-vpc
    vpc_id = aws_vpc.development-vpc.id
    # calling the variable
    # cidr_block = var.subnet_cidr_block

    # calling the varianle from a list of values
    cidr_block = var.cidr_block[1]

    availability_zone = var.avail_zone

    tags = {
      "Name" = "subnet-1-dev"
    }
}

resource "aws_subnet" "prod-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    
    # accssing subnet from list of object
    cidr_block = var.prod_cidr_block_object[0].cidr_block
    availability_zone = "us-east-1b"

    tags = {
      "Name" = var.prod_cidr_block_object[0].name
    }
  
}

# Data Source: query existing resources

# data "aws_vpc" "existing_vpc" {
#     default = true
# }

# # create subnet inside the default VPC
# resource "aws_subnet" "dev-subnet-2" {
#     # referencing the query of the data source
#     vpc_id = data.aws_vpc.existing_vpc.id
#     cidr_block = "172.31.96.0/20"
#     availability_zone = "us-east-1b"

#     tags = {
#       "Name" = "subnet-2-dev"
#     }
  
# }



# output: What value we want terraform to print out after it make changes

output "dev-vpc-id" {
    value = aws_vpc.development-vpc.id
  
}
output "dev-subnet1-id" {
    value = aws_subnet.dev-subnet-1.id
  
}
output "prod-subnet-1" {
    value = aws_subnet.prod-subnet-1.id
  
}